import React, { Component } from "react";
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button';
import axios from 'axios';
//client/components/Update.js
import Modal from 'react-modal';
import { Link } from 'react-router-dom';
var querystring = require('querystring');

export default class EditAssignement extends Component {

  constructor(props) {
    super(props)

    this.onChangeAssignementName = this.onChangeAssignementName.bind(this);
    this.onChangeAssignementMatiere = this.onChangeAssignementMatiere.bind(this);
    this.onChangeAssignementRemarque = this.onChangeAssignementRemarque.bind(this);
    this.onChangeAssignementNote = this.onChangeAssignementNote.bind(this);
    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    // State
    this.state = {
      name: '',
      matiere: '',
      remarque: '',
      note: '',
      messageFromServer: '',
      modalIsOpen: false
    }
  }

  openModal() {
    this.setState({
      modalIsOpen: true
    });
  }
closeModal() {
    this.setState({
      modalIsOpen: false,
      messageFromServer: ''
    });
  }

  componentDidMount() {
    axios.get('http://localhost:4000/assignements/edit-assignement/' + this.props.match.params.id)
      .then(res => {
        this.setState({
          name: res.data.name,
          matiere: res.data.matiere,
          remarque: res.data.remarque,
          note: res.data.note
        });
      })
      .catch((error) => {
        console.log(error);
      })
  }

  onChangeAssignementName(e) {
    this.setState({ name: e.target.value })
  }

  onChangeAssignementMatiere(e) {
    this.setState({ matiere: e.target.value })
  }

  onChangeAssignementRemarque(e) {
    this.setState({ remarque: e.target.value })
  }

  onChangeAssignementNote(e) {
    this.setState({ note: e.target.value })
  }

  onSubmit(e) {
    e.preventDefault()

    const assignementeObject = {
      name: this.state.name,
      matiere: this.state.matiere,
      remarque: this.state.remarque,
      note: this.state.note
    };

    axios.put('http://localhost:4000/assignements/update-assignement/' + this.props.match.params.id, assignementeObject)
      .then((res) => {
        console.log(res.data)
        console.log('Assignement successfully updated')
      }).catch((error) => {
        console.log(error)
      })

    // Redirect to Assignement List 
   //this.props.history.push('/assignement-list')
  }


  render() {
    return (<div className="form-wrapper">
    <Button bsStyle="warning" bsSize="small" onClick={this.openModal}><span className="glyphicon glyphicon-edit"></span></Button>
    <Modal
            isOpen={this.state.modalIsOpen}
            onRequestClose={this.closeModal}
            contentLabel="Add Expense"
            className="Modal">

<Link to={{pathname: '/', search: '' }} style={{ textDecoration: 'none' }}>
            <Button bsStyle="danger" bsSize="mini" onClick={this.closeModal}><span className="closebtn glyphicon glyphicon-remove"></span></Button>
          </Link><br/>
          <fieldset>

      <Form onSubmit={this.onSubmit}>
        <Form.Group controlId="Name">
          <Form.Label>Name</Form.Label>
          <Form.Control type="text" value={this.state.name} onChange={this.onChangeAssignementName} />
        </Form.Group>

        <Form.Group controlId="Email">
          <Form.Label>Matiere</Form.Label>
          <Form.Control type="text" value={this.state.matiere} onChange={this.onChangeAssignementMatiere} />
        </Form.Group>

        <Form.Group controlId="Name">
          <Form.Label>Remarque</Form.Label>
          <Form.Control type="text" value={this.state.remarque} onChange={this.onChangeAssignementRemarque} />
        </Form.Group>

        <Form.Group controlId="Name">
          <Form.Label>Note</Form.Label>
          <Form.Control type="text" value={this.state.note} onChange={this.onChangeAssignementNote} />
        </Form.Group>

        <Button variant="danger" size="lg" onClick={this.onSubmit} block="block" type="submit">
          Update Assignement
        </Button>
      </Form> </fieldset>

          </Modal>
    </div>);
  }
}
