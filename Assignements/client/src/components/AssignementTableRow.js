import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import Button from 'react-bootstrap/Button';


export default class AssignementTableRow extends Component {

    constructor(props) {
        super(props);
        this.deleteAssignement = this.deleteAssignement.bind(this);
    }

    deleteAssignement() {
        axios.delete('http://localhost:4000/Assignements/delete-assignement/' + this.props.obj._id)
            .then((res) => {
                console.log('Assignemet successfully deleted!')
            }).catch((error) => {
                console.log(error)
            })
    }

    render() {
        return (
            <tr>
                <td>{this.props.obj.name}</td>
                <td>{this.props.obj.matiere}</td>
                <td>{this.props.obj.remarque}</td>
                <td>{this.props.obj.note}</td>
                <td>
                    <Link className="edit-link" to={"/edit-assignement/" + this.props.obj._id}>
                        Edit
                    </Link>
                    <Link className="edit-link" to={"/detail-assignement/" + this.props.obj._id}>
                        Detail
                    </Link>
                    <Button onClick={this.deleteAssignement} size="sm" variant="danger">Delete</Button>
                </td>
            </tr>
        );
    }
}