import React, { Component } from "react";


import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button';

import axios from 'axios';
export default class CreateAssignements extends Component {

  constructor(props) {
    super(props)

    // Setting up functions
    this.onChangeAssignementName = this.onChangeAssignementName.bind(this);
    this.onChangeAssignementMatiere = this.onChangeAssignementMatiere.bind(this);
    this.onChangeAssignementRemarque = this.onChangeAssignementRemarque.bind(this);
    this.onChangeAssignementNote = this.onChangeAssignementNote.bind(this);
    this.onChangeAssignementMatIm = this.onChangeAssignementMatIm.bind(this);

    this.onSubmit = this.onSubmit.bind(this);

    // Setting up state
    this.state = {
      name: '',
      remarque: '',
      matiere: '',
      note: '',
      matIm: '',
      profIm: '',
      assignements:[],
      matieres: []
    }
  }
  componentDidMount() {
    this.setState({
      matieres: [
        {nom: 'Gails', prof: 'Gregory'},
        {nom: 'java', prof: 'Amoss'},
        {nom: 'javascript', prof: 'Buffa'},
        {nom: 'DBA', prof: 'Mopolo'}
      ]
    });
  }


  onChangeAssignementName(e) {
    this.setState({ name: e.target.value })
  }

  onChangeAssignementMatiere(e) {

    this.setState({ matiere: e.target.value })

      switch (e.target.value) {
        case "Grail": 
        this.setState({ prof: "Gregory" })
        break
        case "java": 
        this.setState({ prof: "Amos" })
        break
        case "javascrip":
        this.setState({ prof: "BUFFA" })
        break
        case "dba":
        this.setState({ prof: "MOPOLO" })
       
      }

    }


  onChangeAssignementRemarque(e) {
    this.setState({ remarque: e.target.value })
  }
  
  onChangeAssignementNote(e) {
    this.setState({ note: e.target.value })
  }
  onChangeAssignementMatIm(e) {
    this.setState({ matIm: e.target.files[0] })
  }


  onSubmit(e) {
    e.preventDefault()
      const assignementObject = {
        name: this.state.name,
        matiere: this.state.matiere,
        prof:  this.state.prof,
        note: this.state.note,
        remarque: this.state.remarque,
        matIm: this.state.matIm,
      }
      console.log(assignementObject)
      axios.post('http://localhost:4000/Assignements/create-assignement', assignementObject)
      .then(res => console.log(res.data));
      this.setState({
        name: '',
        note: '',
        remarque: ''
      });
    }
/*

  <Form.Group controlId="Name">
          <Form.Label>matiere</Form.Label>
          <Form.Control type="text" value={this.state.matiere} onChange={this.onChangeAssignementMatiere} />
        </Form.Group>
*/
 
  render() {
    return (<div className="form-wrapper">
      <Form onSubmit={this.onSubmit}>

      <input 
                type="file" 
                accept=".png, .jpg, .jpeg"
                name="matIm"
                onChange={this.onChangeAssignementMatIm}
            />
        <Form.Group controlId="Name">
          <Form.Label>Name</Form.Label>
          <Form.Control type="text" value={this.state.name} onChange={this.onChangeAssignementName} />
        </Form.Group>
       
        <select value={this.state.matiere} onChange={this.onChangeAssignementMatiere}>
            {this.state.matieres.map((i) => (
              <option value={i.value} >{i.nom}</option>
            ))}
          </select>

        <Form.Group controlId="Name">
          <Form.Label>Remarque</Form.Label>
          <Form.Control type="text" value={this.state.remarque} onChange={this.onChangeAssignementRemarque} />
        </Form.Group>

        <Form.Group controlId="Name">
          <Form.Label>Note</Form.Label>
          <Form.Control type="text" value={this.state.note} onChange={this.onChangeAssignementNote} />
        </Form.Group>

        <Button variant="danger" size="lg" block="block" type="submit">
          Create Assignement
        </Button>
      </Form>
    </div>);
  }
}
