import React, { Component } from "react";
import { Card, Button, ListGroup, ListGroupItem} from 'react-bootstrap';
import axios from 'axios';

export default class DetailAssignement extends Component {

  constructor(props) {
    super(props)

    this.onChangeAssignementName = this.onChangeAssignementName.bind(this);
    this.onChangeAssignementMatiere = this.onChangeAssignementMatiere.bind(this);
    this.onChangeAssignementRemarque = this.onChangeAssignementRemarque.bind(this);
    this.onChangeAssignementNote = this.onChangeAssignementNote.bind(this);

    // State
    this.state = {
      name: '',
      matiere: '',
      remarque: '',
      note: '',
    }
  }
 
  
  onChangeAssignementName(e) {
    this.setState({ name: e.target.value })
  }

  onChangeAssignementMatiere(e) {
    this.setState({ matiere: e.target.value })
  }

  onChangeAssignementRemarque(e) {
    this.setState({ remarque: e.target.value })
  }

  onChangeAssignementNote(e) {
    this.setState({ note: e.target.value })
  };
  
  componentDidMount() {
    { 
    axios.get('http://localhost:4000/assignements/edit-assignement/' + this.props.match.params.id)
      .then(res => {
        this.setState({
          name: res.data.name,
          matiere: res.data.matiere,
          remarque: res.data.remarque,
          note: res.data.note
        });
      })
      .catch((error) => {
        console.log(error);
      })
    }
  }




  render() {
    return (<div className="form-wrapper">
      <Card style={{ width: '18rem' }}>
  <Card.Img variant="top" src="holder.js/100px180?text=Image cap" />
  <Card.Body>
    <Card.Title>Card Title</Card.Title>
    <Card.Text>
      Some quick example text to build on the card title and make up the bulk of
      the card's content.
    </Card.Text>
  </Card.Body>
  <ListGroup className="list-group-flush">
    <ListGroupItem>{this.state.name}</ListGroupItem>
    <ListGroupItem>{this.state.matiere}</ListGroupItem>
    <ListGroupItem>{this.state.remarque}</ListGroupItem>
    <ListGroupItem>{this.state.note}</ListGroupItem>
  </ListGroup>
  <Card.Body>
  <Button variant="danger" href={"/assignement-list"} size="lg" block="block" type="submit">
          ok
        </Button>
  </Card.Body>
</Card>
 </div>);
  }
}
