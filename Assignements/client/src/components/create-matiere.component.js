import React, { Component } from "react";


import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button';

import axios from 'axios';
export default class CreateMatieres extends Component {

  constructor(props) {
    super(props)

    // Setting up functions
    this.onChangeMatiereNom = this.onChangeMatiereNom.bind(this);
    this.onChangeMatierePhotoMat = this.onChangeMatierePhotoMat.bind(this);
    this.onChangeMatiereProf = this.onChangeMatiereProf.bind(this);
    this.onChangeMatierePhotoProf = this.onChangeMatierePhotoProf.bind(this);

    this.onSubmit = this.onSubmit.bind(this);

    // Setting up state
    this.state = {
      nom: '',
      photoMat: '',
      prof: '',
      photoProf: '',
      assigmentMat:[]
    }
  }
  componentDidMount() {
    axios.get('http://localhost:4000/assigmentMat/:matiere')
      .then(res => {
        this.setState({
          assigmentMat: res.data,     
        });
      })
      .catch((error) => {
        console.log(error);
      })
  };


  onChangeMatiereNom(e) {
    this.setState({ nom: e.target.value })
  }

  onChangeMatierePhotoMat(e) {
    this.setState({ photoMat: e.target.value })

      switch (e.target.value) {
        case "Grail": 
        this.setState({ prof: "Gregory" })
        break
        case "java": 
        this.setState({ prof: "Amos" })
        break
        case "javascrip":
        this.setState({ prof: "BUFFA" })
        break
        case "dba":
        this.setState({ prof: "MOPOLO" })
       
      }

    }


  onChangeMatiereProf(e) {
    this.setState({ prof: e.target.value })
  }
  
  onChangeMatierePhotoProf(e) {
    this.setState({ photoProf: e.target.value })
  }


  onSubmit(e) {
    e.preventDefault()
      const matiereObject = {
        nom: this.state.nom,
        photoMat: this.state.photoMat,
        photoProf: this.state.photoProf,
        prof: this.state.prof,
      }
   
      axios.post('http://localhost:4000/assigmentMat/create-matiere', matiereObject)
      .then(res => console.log(res.data));
    }

 
  render() {
    return (<div className="form-wrapper">
      <Form onSubmit={this.onSubmit}>
        <Form.Group controlId="Name">
          <Form.Label>prof</Form.Label>
          <Form.Control type="text" value={this.state.prof} onChange={this.onChangeMatiereProf} />
        </Form.Group>
       
  

          <Form.Group controlId="Name">
          <Form.Label>Nom</Form.Label>
          <Form.Control type="text" value={this.state.nom} onChange={this.onChangeMatiereNom} />
        </Form.Group>

        <Form.Group controlId="Name">
          <Form.Label>photoMat</Form.Label>
          <Form.Control type="text" value={this.state.photoMat} onChange={this.onChangeMatierePhotoMat} />
        </Form.Group>

        <Form.Group controlId="Name">
          <Form.Label>PhotoProf</Form.Label>
          <Form.Control type="text" value={this.state.photoProf} onChange={this.onChangeMatierePhotoProf} />
        </Form.Group>

        <Button variant="danger" size="lg" block="block" type="submit">
          Create matiere
        </Button>
      </Form>
    </div>);
  }
}
