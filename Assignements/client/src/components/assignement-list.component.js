import React, { Component } from "react";
import axios from 'axios';
import Table from 'react-bootstrap/Table';
import AssignementTableRow from './AssignementTableRow';
export default class AssignementList extends Component {

  constructor(props) {
    super(props)
    this.state = {
      assignements: [],
      loading: false,
      page: 0,
      prevY: 0
    };
  }

  getAssignements(page) {
    this.setState({ loading: true });
    axios.get(`http://localhost:4000/Assignements/`)
    //?_page=${page}&_limit=1
      .then(res => {
        this.setState({ assignements: [...this.state.assignements, ...res.data] });
        this.setState({ loading: false });
      });
  }

  componentDidMount() {
    this.getAssignements(this.state.page);
    
    this.observer = new IntersectionObserver(
      this.handleObserver.bind(this),
    );
    this.observer.observe(this.loadingRef);
  }

  handleObserver(entities, observer) {
    const y = entities[0].boundingClientRect.y;
    if (this.state.prevY > y) {
      const lastPhoto = this.state.assignements[this.state.assignements.length - 1];
      const curPage = lastPhoto
      this.getAssignements(curPage);
      this.setState({ page: curPage });
    }
    this.setState({ prevY: y });
  }


  DataTable() {
    return this.state.assignements.map((res, i) => {
      if(res.note!=0 && res.note!=null){  
        return <AssignementTableRow obj={res} key={i} />;
        }
  
    });
  }



  DataTable2() {
    return this.state.assignements.map((res, i) => {
      if(res.note==0 || res.note== null ){  
      return <AssignementTableRow obj={res} key={i} />;
      }
    });
  }


  render() {
    return (
      <div className="row">        
    <div className="col table-wrapper">
    <b>Assignement Rendu</b>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Name</th>
            <th>Matiere</th>
            <th>Remarque</th>
            <th>Note</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {this.DataTable()}
        </tbody>
      </Table>
<div
        ref={loadingRef => (this.loadingRef = loadingRef)} >
      </div>
    </div>


    <div className="col table-wrapper">
    <b>Assignement Non Rendu</b>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Name</th>
            <th>Matiere</th>
            <th>Remarque</th>
            <th>Note</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {this.DataTable2()}
        </tbody>
      </Table>
      <div
        ref={loadingRef => (this.loadingRef = loadingRef)} >
      </div>
    </div>
   
    
    </div>);
  }

  
}