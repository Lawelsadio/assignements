import React from "react";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import "bootstrap/dist/css/bootstrap.css";
import "./App.css";

import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

import CreateAssignements from "./components/create-assignement.component";
import EditAssignement from "./components/edit-assignement.component";
import DetailAssignement from "./components/detail-assignement.component";
import AssignementList from "./components/assignement-list.component";

function App() {
  return (<Router>
    <div className="App">
      <header className="App-header">
        <Navbar bg="dark" variant="dark">
          <Container>

            <Navbar.Brand>
              <Link to={"/create-assignement"} className="nav-link">
              Assignements
              </Link>
            </Navbar.Brand>

            <Nav className="justify-content-end">
              <Nav>
                <Link to={"/create-assignement"} className="nav-link">
                  Create Assignement
                </Link>
              </Nav>

              <Nav>
                <Link to={"/assignement-list"} className="nav-link">
                AssignementList 
                </Link>
              </Nav>
            </Nav>

          </Container>
        </Navbar>
      </header>

      <Container>
        <Row>
          <Col md={12}>
            <div className="wrapper">
              <Switch>
                <Route exact path='/' component={CreateAssignements} />
                <Route path="/create-assignement" component={CreateAssignements} />
                <Route path="/edit-assignement/:id" component={EditAssignement} />
                <Route path="/detail-assignement/:id" component={DetailAssignement} />
                <Route path="/assignement-list" component={AssignementList} />
              </Switch>
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  </Router>);
}

export default App;