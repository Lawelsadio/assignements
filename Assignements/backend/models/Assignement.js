const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Matiere = Object.freeze({
  Grails: 'Grails',
  java: 'java',
  javascrip: 'javascript',
  DBA: 'DBA',
})

let assigmentShemat = new Schema({
  name: {
    type: String
  },
  matiere: {
    type: String,
    enum: Object.values(Matiere),
  },
  remarque: {
    type: String
  },
  prof: {
    type: String
  },
  note: {
    type: Number
  },
  matIm: {
    data: Buffer,
        contentType: String
  }
}, {
    collection: 'assigments'
  })

module.exports = mongoose.model('Assignement', assigmentShemat)