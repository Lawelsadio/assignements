let mongoose = require('mongoose'),
  express = require('express'),
  router = express.Router();

// Assignement Model
let assigmentShemat = require('../models/Assignement');


// CREATE Assignements
router.route('/create-assignement').post((req, res, next) => {
  assigmentShemat.create(req.body, (error, data) => {
    if (error) {
      return next(error)
    } else {
      console.log(data)
      res.json(data)
    }
  })
});





// READ Assignement/ 
router.route('/').get((req, res, next) => {
  assigmentShemat.find((error, data) => {
    if (error) {
      return next(error)
    } else {
      res.json(data)
    }
  })
})

/*
// READ Assignement/ 
router.route('/').get((req, res) => {
  matiereShemat.find((error, data) => {
    if (error) {
      return next(error)
    } else {
      res.json(data)
    }
  })
})
*/

// READ Assignement/ 
router.route('/:matiere').get((req, res,next) => {
  assigmentShemat.find((error, data) => {
    if (error) {
      return next(error)
    } else {
      res.json(data)
    }
  })
})

// ! pensser a recuperer avec le id les matieres pour ensuite les copiers dans l asseignement


// Get Single Assignement
router.route('/edit-assignement/:id').get((req, res) => {
  assigmentShemat.findById(req.params.id, (error, data) => {
    if (error) {
      return next(error)
    } else {
      res.json(data)
    }
  })
})

// Get Single Assignement
router.route('/detail-assignement/:id').get((req, res) => {
  assigmentShemat.findById(req.params.id, (error, data) => {
    if (error) {
      return next(error)
    } else {
      res.json(data)
    }
  })
})


// Update Assignement
router.route('/update-assignement/:id').put((req, res, next) => {
  assigmentShemat.findByIdAndUpdate(req.params.id, {
    $set: req.body
  }, (error, data) => {
    if (error) {
      return next(error);
      console.log(error)
    } else {
      res.json(data)
      console.log('assignement updated successfully !')
    }
  })
})

// Delete Assignement
router.route('/delete-assignement/:id').delete((req, res, next) => {
  assigmentShemat.findByIdAndRemove(req.params.id, (error, data) => {
    if (error) {
      return next(error);
    } else {
      res.status(200).json({
        msg: data
      })
    }
  })
})

module.exports = router;